<%--
  Created by IntelliJ IDEA.
  User: Aleksandr
  Date: 18.05.2016
  Time: 21:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>CHAT</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <link rel="stylesheet" href="/WEB-INF/css/main.css">
</head>
<body>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<div class="container">
    <div class="row" style="padding-top:40px;">
        <h3 class="text-center">HI, Welcome to our chat!</h3>
        <br/><br/>
        <div class="col-md-8">
            <div class="panel panel-info">
                <div class="panel-heading">
                    RECENT CHAT HISTORY
                </div>
                <div class="panel-body">
                    <ul class="media-list">
                        <c:forEach var="item" items= "${queue}" >
                            <li class="media">
                                <div class="media-body">
                                    <div class="media">
                                        <a class="pull-left" href="#">
                                            <img class="media-object img-circle " src="http://lorempixel.com/50/50"/>
                                        </a>
                                        <div class="media-body">
                                                ${item.text}
                                            <br/>
                                            <small class="text-muted">${item.author} | ${item.time}</small>
                                            <hr/>
                                        </div>
                                    </div>
                                </div>
                            </li>

                        </c:forEach>


                    </ul>
                </div>
                <div class="panel-footer">
                    <form method="post" action="${contextPath}/thechat">
                        <div class="input-group" >
                            <input id="text_in" name="text_in" type="text" class="form-control" placeholder="Enter Message"/>
                                    <span class="input-group-btn">
                                        <button class="btn btn-info" type="submit">SEND</button>
                                    </span>
                        </div>
                    </form>
                </div>
            </div>
            <div>
                <span id="text_out" name="text_out">
                    ${serverTime}
                </span>
            </div>
        </div>
        <div class="col-md-4">

            <div class="panel panel-primary">
                <div class="panel-heading">
                    Please sign in
                </div>
                <form class="form-signin" style="margin-bottom: 20px; padding: 10px">
                    <label for="inputEmail" class="sr-only">Email address</label>
                    <input type="email" id="inputEmail" class="form-control" placeholder="Email address" required
                           autofocus>
                    <label for="inputPassword" class="sr-only">Password</label>
                    <input type="password" id="inputPassword" class="form-control" placeholder="Password" required>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" value="remember-me"> Remember me
                        </label>
                    </div>
                    <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
                </form>
            </div>

            <div class="panel panel-primary">
                <div class="panel-heading">
                    ONLINE USERS
                </div>
                <div class="panel-body">
                    <ul class="media-list">

                        <li class="media">

                            <div class="media-body">

                                <div class="media">
                                    <a class="pull-left" href="#">
                                        <img class="media-object img-circle" style="max-height:40px;"
                                             src="http://lorempixel.com/50/50"/>
                                    </a>
                                    <div class="media-body">
                                        <h5>Alex Deo | User </h5>

                                        <small class="text-muted">Active From 3 hours</small>
                                    </div>
                                </div>

                            </div>
                        </li>
                        <li class="media">

                            <div class="media-body">

                                <div class="media">
                                    <a class="pull-left" href="#">
                                        <img class="media-object img-circle" style="max-height:40px;"
                                             src="http://lorempixel.com/50/50"/>
                                    </a>
                                    <div class="media-body">
                                        <h5>Jhon Rexa | User </h5>

                                        <small class="text-muted">Active From 3 hours</small>
                                    </div>
                                </div>

                            </div>
                        </li>
                        <li class="media">

                            <div class="media-body">

                                <div class="media">
                                    <a class="pull-left" href="#">
                                        <img class="media-object img-circle" style="max-height:40px;"
                                             src="http://lorempixel.com/50/50"/>
                                    </a>
                                    <div class="media-body">
                                        <h5>Alex Deo | User </h5>

                                        <small class="text-muted">Active From 3 hours</small>
                                    </div>
                                </div>

                            </div>
                        </li>
                        <li class="media">

                            <div class="media-body">

                                <div class="media">
                                    <a class="pull-left" href="#">
                                        <img class="media-object img-circle" style="max-height:40px;"
                                             src="http://lorempixel.com/50/50"/>
                                    </a>
                                    <div class="media-body">
                                        <h5>Jhon Rexa | User </h5>

                                        <small class="text-muted">Active From 3 hours</small>
                                    </div>
                                </div>

                            </div>
                        </li>
                        <li class="media">

                            <div class="media-body">

                                <div class="media">
                                    <a class="pull-left" href="#">
                                        <img class="media-object img-circle" style="max-height:40px;"
                                             src="http://lorempixel.com/50/50"/>
                                    </a>
                                    <div class="media-body">
                                        <h5>Alex Deo | User </h5>

                                        <small class="text-muted">Active From 3 hours</small>
                                    </div>
                                </div>

                            </div>
                        </li>

                    </ul>
                </div>
            </div>

        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-1.12.3.min.js"
        integrity="sha256-aaODHAgvwQW1bFOGXMeX+pC4PZIPsvn2h1sArYOhgXQ=" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"
        integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS"
        crossorigin="anonymous"></script>
<script src="/WEB-INF/js/main.js"></script>
</body>
</html>
