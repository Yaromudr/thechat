package ru.goblin.learning.dto;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by goblin on 22.05.16.
 */
public class Message {
    private String author;
    private String text;
    private String time;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");

    public Message(String author, String text) {
        this.author = author;
        this.text = text;
        time = dateFormat.format(new Date());
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
