package ru.goblin.learning.contollers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.goblin.learning.dto.Message;

import java.util.LinkedList;
import java.util.Queue;


/**
 * Created by goblin on 22.05.16.
 */
@Controller
public class MassageController {

    private Queue<Message> queue = new LinkedList<Message>();
    private int maxSize = 5;

    private static String CHAT_PAGE = "chat";

    @RequestMapping(value = "/thechat", method = RequestMethod.POST)
    public String sendToPost(@RequestParam String text_in,  Model model) {

        if(queue.size()==maxSize){
            queue.remove();
        }

        queue.add(new Message("Dummy", text_in));
        model.addAttribute("queue", queue);
        return CHAT_PAGE;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String sendToGet(Model model) {
        queue.clear();
        queue.add(new Message("Бот", "Welcome to Hell!"));
        model.addAttribute("queue", queue);
        return CHAT_PAGE;
    }
}
