package ru.goblin.learning.contollers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import static java.lang.String.format;

@Controller
public class TestPageController {

    private static final Logger logger = LoggerFactory.getLogger(TestPageController.class);

    /**
     * Simply selects the home view to render by returning its name.
     */
    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public String home(Locale locale, Model model) {
        logger.info(format("Welcome home! The client locale is %s.", locale));

        Date date = new Date();
        DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);

        String formattedDate = dateFormat.format(date);

        model.addAttribute("serverTime", formattedDate );

        return "testPage";
    }

}
