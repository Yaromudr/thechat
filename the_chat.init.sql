SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

DROP SCHEMA IF EXISTS `the_chat` ;
CREATE SCHEMA IF NOT EXISTS `the_chat` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `the_chat` ;

-- -----------------------------------------------------
-- Table `project_simple`.`user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `the_chat`.`user` ;

CREATE TABLE IF NOT EXISTS `the_chat`.`user` (
  `id` INT NOT NULL,
  `first_name` VARCHAR(35) NULL,
  `last_name` VARCHAR(35) NULL,
  `login` VARCHAR(35) NULL,
  `password` VARCHAR(64) NULL,
  `is_deleted` TINYINT(1) DEFAULT NULL,
  `role_id` INT NOT NULL DEFAULT 1,
  FOREIGN KEY (`role_id`) REFERENCES role(`id`),
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

-- -----------------------------------------------------
-- Table `project_simple`.`message`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `the_chat`.`message` ;

CREATE TABLE IF NOT EXISTS `the_chat`.`message` (
  `id` INT NOT NULL,
  `user_id` INT NOT NULL,
  `message` VARCHAR(255) NULL,
  `dt_created` DATETIME NOT NULL,
  `dt_modified` DATETIME NOT NULL,
  `is_deleted` TINYINT(1) DEFAULT NULL,
  FOREIGN KEY (`user_id`) REFERENCES user(`id`),
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

-- -----------------------------------------------------
-- Table `project_simple`.`role`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `the_chat`.`role` ;

CREATE TABLE IF NOT EXISTS `the_chat`.`role` (
  `id` INT NOT NULL,
  `role` VARCHAR(35) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;


-- -----------------------------------------------------
-- Data for table `the_chat`.`role`
-- -----------------------------------------------------
START TRANSACTION;
USE `the_chat`;
INSERT INTO `the_chat`.`role` (`id`, `role`) VALUES (1, 'user');
INSERT INTO `the_chat`.`role` (`id`, `role`) VALUES (2, 'admin');
    

COMMIT;


-- -----------------------------------------------------
-- Data for table `the_chat`.`user`
-- -----------------------------------------------------
START TRANSACTION;
USE `the_chat`;
INSERT INTO `the_chat`.`user` 
	(`id`, `first_name`, `last_name`, `login`, `password`, `is_deleted`, `role_id`) 
    VALUES (1, 'Stepan', 'Pelmegov', 'Stepan', '123', NULL, 2);
INSERT INTO `the_chat`.`user` 
	(`id`, `first_name`, `last_name`, `login`, `password`, `is_deleted`, `role_id`) 
    VALUES (2, 'Alexander', 'Dolinsky', 'Alexander', '123', NULL, 2);
INSERT INTO `the_chat`.`user` 
	(`id`, `first_name`, `last_name`, `login`, `password`, `is_deleted`, `role_id`) 
    VALUES (3, 'Dmitry', 'Zhezlov', 'Jooble', '123', NULL, 1);
INSERT INTO `the_chat`.`user` 
	(`id`, `first_name`, `last_name`, `login`, `password`, `is_deleted`, `role_id`) 
    VALUES (4, 'Dmitry', ' Belousov', 'Dmitry', '123', NULL, 1);

COMMIT;


-- -----------------------------------------------------
-- Data for table `the_chat`.`message`
-- -----------------------------------------------------
START TRANSACTION;
USE `the_chat`;
INSERT INTO `the_chat`.`message` 
	(`id`, `user_id`, `message`, `dt_created`, `dt_modified`, `is_deleted`) 
    VALUES (1, 1, 'Hello World!', '14.02.2015', '15.02.2015', NULL);
INSERT INTO `the_chat`.`message` 
	(`id`, `user_id`, `message`, `dt_created`, `dt_modified`, `is_deleted`) 
    VALUES (2, 2, 'Hi guys!', '10.04.2015', '13.04.2015', NULL);
INSERT INTO `the_chat`.`message` 
	(`id`, `user_id`, `message`, `dt_created`, `dt_modified`, `is_deleted`) 
    VALUES (3, 3, 'I love Java!', '22.02.2016', '24.02.2016', NULL);
INSERT INTO `the_chat`.`message` 
	(`id`, `user_id`, `message`, `dt_created`, `dt_modified`, `is_deleted`) 
    VALUES (4, 2, 'Oh my god. I love Java TOO!', '22.02.2016', '24.02.2016', NULL);


COMMIT;